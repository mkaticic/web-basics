/* index.js */
$(document).ready(function(){
  
  "use strict"; 
  
  /**/
  $("#gitHubSearchForm").on("submit",
      function(){
	/* dohvati parametre iz forme */
	var searchPhrase = $("#searchPhrase").val();
	var useStars = $("#useStars").val();
	var langChoice = $("#langChoice").val();
	
	if(searchPhrase){
	  resultList.text = "Preforming search...";
	  
	  /* preko get metode jQueryja pokrećemo pretragu te definiramo callback funkciju koja će obraditi rezultat mrežnog get upita*/
	  var githubSearch = "https://api.github.com/search/repositories?q=" + encodeURIComponent(searchPhrase);
	  if(langChoice != "All"){
	    githubSearch += "language:" + encodeURIComponent(langChoice); // encodeURIComponent - moramo encodirati spc. znakove iz inputa da bi ih mogli koristiti kao parametar u GET-u
	  }
	  if(useStars){
	    githubSearch += "&sort=stars";
	  }
	  
	  $.get(githubSearch)
	  .success(function(r){ /*clallback*/
		displayResults(r.items);
	      })
	  .fail(function(r){
		console.log("Call to " + githubSearch + " failed");
	      })
	  .done(function(){
		console.log("Call to " + githubSearch + " succeded");
	  });
	}
	
	return false;	// spriječi pozivanje linka iz forme!!
      }
  );
  

  /* dohvati objekt sa id-jem resultList - jQuery koristi css selektore za dohvat objekata*/
  var resultList = jQuery("#resultList");
  resultList.text("this is from jQuery");

  var toggleButton = $("#toggleButton");
  
  /* 'on' event method - Attaches event handlers to elements
   * 'click' event
   * execute defined callback function
   */
  toggleButton.on(
    "click", 
    function(){
      resultList.toggle(500); // toggle function: if it's shown - hide it, if it's hidden - show it
      if(toggleButton.text() == "Hide") toggleButton.text("Show")
      else toggleButton.text("Hide");
    }
  ); 
  
  
  var listItems = $("header nav li");
  listItems.css("font-weight", "bold");
  listItems.filter(":first").css("font-size", "18px");
  
  /*
  var results = [];
  results.push(
      {
      name: "jQuery",
      language: "JavaScript",
      score: 4.5,
      showLog: function (){
      },
      owner:{
	login: "mkaticic",
	id: 123456
      }
    },

    {
      name: "jQuery UI",
      language: "JavaScript",
      score: 3.5,
      showLog: function (){
      },
      owner:{
	login: "mkaticic",
	id: 123456
      }
    }
  );
*/
  
  function displayResults(results){
    resultList.empty();
    $.each(results, function(i, item){
	var newR = $(
	  "<div class='result'>" +
	  "<div><b>Project name: " + item.name + "</b></div>" +
	  "<div>Language: " + item.language + "</div>" +
	  "<div>Owner: " + item.owner.login + "</div>" +
	  "</div>"
	);
	
	newR.hover(
	  function(){
	    $(this).css("background-color", "lightgrey");
	  },
	  function(){
	    $(this).css("background-color", "transparent");
	  }
	);
    
	resultList.append(newR);
    });
  }

})
/*
var msg="hello javascript";
//alert(msg);
console.log(msg);

var resultsDiv = document.getElementById("results");
resultsDiv.innerHTML = "<p>This is from js</p>"; 

var result = {
  name: "jQuery",
  language: "JavaScript",
  score: 4.5,
  showLog: function (){
  },
  owner:{
    login: "mkaticic",
    id: 123456
  }
};
result.phoneNumber="865-114";



for(var x=0; x< results.length; x++){
  var result = results[x];
  if(result.score > 4) continue;
  console.log(result.name);
}
*/



/*
console.log("msg is " + typeof(msg));
console.log("resultsDiv is " + typeof(resultsDiv));

var none;
console.log("none is " + typeof(none));

var booleanT = true;
console.log("booleanT is " + typeof(booleanT));

//nonexistent = "this shouldn't work"; 

if (none == undefined){
  console.log("none is undefined");
}

var aNumber = 10;
if(aNumber == "10"){
  console.log("js prije usporedbe sve tipove pretvara u zajednički tip koji se može uspoređivati!");
}

if(aNumber === "10"){
  console.log("ovo se ne bi trebalo ispisat!");
}

if(aNumber !== "10"){
  console.log("a ovo se bi trebalo");
}

//function showMsg(msg){
//  console.log("msg: " + msg);
//}

function showMsg(msg, more){
  if(more){
    console.log("msg: " + msg +  " " + more);
  }else{
    console.log("msg: " + msg);
  }
}

showMsg("moja poruka");
showMsg("overloading", "nije podržan!!");

var showIt = function(msg){
  console.log(msg);
} 

function fjaKaoParametar(msg, fja){
  showIt(msg);
  fja();
}

fjaKaoParametar("blalala", function (){console.log("izvršavam se")});

window.showMsg("ispisujem se preko window objekta");

*/